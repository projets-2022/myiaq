/**
 * Démo mise en oeuvre DAC Audio i2s
 *
 * Voir https://www.lycee-benoit.tech/BTS/_defrance/e6-2/myiaq/i2s.html pour les configurations 
 * à faire au préalable
 */
#include <QCoreApplication>
#include <QObject>

#include <QMediaPlayer>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // Instanciation du lecteur audio
    QMediaPlayer * player = new QMediaPlayer;

    // Mise en place d'un slot qui permet d'afficher la progression de la
    // lecture du son (chargement, fin) et de quitter l'application après
    // sa lecture
    QObject::connect(player, &QMediaPlayer::stateChanged, [=]() {
        QMediaPlayer::MediaStatus state = player->mediaStatus();
        qDebug() << "status " << state;
        if (state == QMediaPlayer::EndOfMedia) {
            QCoreApplication::quit();
        }
    });

    // Spécification du son à jouer
    // (son récupéré sur https://lasonotheque.org/)
    player->setMedia(QUrl::fromLocalFile("/home/pi/Music/2153.wav"));

    // Réglage du volume sonore
    player->setVolume(50);

    // Lecture du son
    player->play();

    return a.exec();
}
