﻿#ifndef CUBECELL_H
#define CUBECELL_H

#include <QObject>
#include <QVector>

#include "ttn.h"
#include "cubecellworker.h"

class Cubecell : public TTN
{
    Q_OBJECT
public:
    explicit Cubecell(QString serialPort, QObject *parent = nullptr);
    void wakeUp();
    void getChipID();
    void getDevEui();
    void getAppEui();
    void getAppKey();
    void getDutyCycle();
    void isTxConfirmed(bool enabled = true);
    void initOTAA(QString appEui, QString appKey, QString devEui = QString()) override;
    void joinOTAA() override;
    void send(QByteArray payload, int port = 1, bool confirm = false) override;

private :
    QString _portName;
    CubecellWorker _workerThread;
    //enum {WakeUp, ChipID, DevEui, AppEui, AppKey, DutyCycle, Join, WaitJoin, SendHex, WaitSendHex} idCmde;
    bool hasHexDigits(const QString &srcStr, QString &hexStr, int len);
    bool hasDecDigits(const QString &srcStr, QString &decStr);
    QString _chipID;

signals:
    void chipIdReady(const QString & s);
    void devEuiReady(const QString & s);
    void appEuiReady(const QString & s);
    void appKeyReady(const QString & s);
    void dutyCycleReady(const QString & s);
    void joinReady(const QString & s);
    void sendHexReady(const QString & s);

private slots:
    void onResponseReceived(const CubecellWorker::cmd_id_t &id, const QString &s);
    void onTimeOut(const CubecellWorker::cmd_id_t &id, const QString &err);

};

#endif // CUBECELL_H
