#ifndef RN2483TTN_H
#define RN2483TTN_H
#include <QSerialPort>
#include <QTimer>

#include "ttn.h"
#include "bcm2835wrapper.h"

/*
Réponses renvoyées par le module RN2483  :
ok
busy
fram_counter_err_rejoin_needed
invalid_class
invalid_data_len
invalid_param
keys_not_init
mac_paused
multicast_keys_not_set
no_free_ch
not_joined
silent
err

denied
accepted
*/
class RN2483TTN : public TTN
{
    Q_OBJECT
public:
    RN2483TTN();
    bool initOTAA(QString appEui, QString appKey, QString devEui = QString()) override;
    bool joinOTAA() override;
    int send(QString payload, int port = 1, bool confirm = false) override;

private:
    typedef enum {TRANSACTION_IN_PROGRESS, NO_RESPONSE} commErr_t;
    QString getHwEui();
    bool sendCmd(QString cmd, QString expectedResp, commErr_t& err);
    bool sendCmd(QString cmd, QString expectedResp = "ok");
    void flushRx(void);
    bool waitForResponse(QString resp);
    void autoBauds();
    void hardReset();

    const uint8_t _RN2483_RESET_GPIO = 18;
    Bcm2835Wrapper& _bcm;
    QSerialPort * _commPort;
    QStringList _respList;
    bool isTransactionInProgress;
    QTimer _delay;
    int _remainingAttempts;


private slots:
    void onReadyRead();
};

#endif // RN2483TTN_H
