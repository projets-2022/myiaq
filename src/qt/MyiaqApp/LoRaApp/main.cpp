#include <QCoreApplication>

#include "loraapp.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    LoraApp lora;

    return a.exec();
}
