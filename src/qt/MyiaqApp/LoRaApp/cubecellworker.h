#ifndef CUBECELLWORKER_H
#define CUBECELLWORKER_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QQueue>
#include <QPair>
#include <QSerialPort>

class CubecellWorker : public QThread
{
    Q_OBJECT
public:
    explicit CubecellWorker(QString portName, QObject *parent = nullptr);
    ~CubecellWorker();
    typedef enum {WAKE_UP, CHIP_ID, DEV_EUI, APP_EUI, APP_KEY, DUTY_CYCLE, JOIN, WAIT_JOIN, SEND_HEX, WAIT_SEND_HEX, AUTO_LPM, IS_TX_CONFIRMED} cmd_id_t;
    void transaction(cmd_id_t idCmd, const QString &param);
    QString getATCommand(cmd_id_t id);

signals:
    void response(const CubecellWorker::cmd_id_t &idCmd, const QString &s);
    void error(const QString &s);
    void timeout(const CubecellWorker::cmd_id_t &idCmd, const QString &s);

private:
    void run() override;

    QMutex _mutex;
    QWaitCondition _cond;
    bool _quit = false;
    QString _serialPortName;

    QQueue<QPair<cmd_id_t, QString>> _msgQueue;
    const QStringList _cmds = {
      "AT+XXX", "AT+ChipID", "AT+DevEui", "AT+AppEui", "AT+AppKey", "AT+DutyCycle", "AT+Join", "", "AT+SendHex", "", "AT+AutoLPM", "AT+IsTxConfirmed"
    };
    const int _CMD_WR_TIMEOUT = 1000; // 1s
    const int _CMD_RD_TIMEOUT = 2000; // 2s
    const int _WAIT_LORA_ACK = 20000; // 20s
    const int _CHUNK_TIMEOUT = 500; // 500ms
};
Q_DECLARE_METATYPE(CubecellWorker::cmd_id_t);

#endif // CUBECELLWORKER_H
