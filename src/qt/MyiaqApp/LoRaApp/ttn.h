#ifndef TTN_H
#define TTN_H

#include <QObject>

class TTN : public QObject
{
    Q_OBJECT
public:
    explicit TTN(QObject *parent = nullptr);

    virtual void initOTAA(QString appEui, QString appKey, QString devEui = QString()) = 0;
    virtual void joinOTAA() = 0;
    virtual void send(QByteArray payload, int port = 1, bool confirm = false) = 0;

protected:
    QString _appEui;
    QString _appKey;
    QString _devEui;

signals:
    void initDone();
    void joined();
    void uplinkDone();

};

#endif // TTN_H
