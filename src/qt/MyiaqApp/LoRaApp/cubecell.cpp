#include <QDebug>
#include <QRegularExpression>

#include "cubecell.h"

Cubecell::Cubecell(QString serialPort, QObject *parent)
    : TTN(parent), _portName(serialPort), _workerThread(serialPort)
{
    // Enregistrement du type cmd_it_t auprès de Qt de façon à pouvoir s'en servir dans un connect()
    qRegisterMetaType<CubecellWorker::cmd_id_t>();

    connect(&_workerThread, &CubecellWorker::response, this, &Cubecell::onResponseReceived);
    connect(&_workerThread, &CubecellWorker::timeout, this, &Cubecell::onTimeOut);

    // Sortir du mode veille
    _workerThread.transaction(CubecellWorker::WAKE_UP, "");

    // Désactiver le mode Auto Low Power
    _workerThread.transaction(CubecellWorker::AUTO_LPM, "0");

    // Récupérer le ChipID
    _chipID.clear();
    _workerThread.transaction(CubecellWorker::CHIP_ID, nullptr);

}

void Cubecell::wakeUp()
{
    _workerThread.transaction(CubecellWorker::WAKE_UP, "");
}

void Cubecell::getChipID()
{
    _workerThread.transaction(CubecellWorker::CHIP_ID, nullptr);
}

void Cubecell::getDevEui()
{
    _workerThread.transaction(CubecellWorker::DEV_EUI, nullptr);
}

void Cubecell::getAppEui()
{
    _workerThread.transaction(CubecellWorker::APP_EUI, nullptr);

}

void Cubecell::getAppKey()
{
    _workerThread.transaction(CubecellWorker::APP_KEY, nullptr);

}

void Cubecell::getDutyCycle()
{
    _workerThread.transaction(CubecellWorker::DUTY_CYCLE, nullptr);

}

void Cubecell::joinOTAA()
{
    _workerThread.transaction(CubecellWorker::JOIN, "1");
    _workerThread.transaction(CubecellWorker::WAIT_JOIN, "");
}

void Cubecell::send(QByteArray payload, int port, bool confirm)
{
    QString data = payload.toHex();
    data = data.toUpper(); // Convertit en majuscule
    data.remove(QChar(' ')); // Retire les espaces éventuels
    if(confirm) {
        _workerThread.transaction(CubecellWorker::IS_TX_CONFIRMED, "1");
    } else {
        _workerThread.transaction(CubecellWorker::IS_TX_CONFIRMED, "0");
    }
    _workerThread.transaction(CubecellWorker::SEND_HEX, data);
    _workerThread.transaction(CubecellWorker::WAIT_SEND_HEX, "");
}

void Cubecell::isTxConfirmed(bool enabled)
{
    if( enabled) {
        _workerThread.transaction(CubecellWorker::IS_TX_CONFIRMED, "1");
    } else {
        _workerThread.transaction(CubecellWorker::IS_TX_CONFIRMED, "0");

    }
}

void Cubecell::initOTAA(QString appEui, QString appKey, QString devEui)
{
    if ( devEui.isEmpty() ) {
        devEui = _chipID;
    }

    _workerThread.transaction(CubecellWorker::APP_EUI, appEui);
    _workerThread.transaction(CubecellWorker::DEV_EUI, devEui);
    _workerThread.transaction(CubecellWorker::APP_KEY, appKey);

    return;
}

/**
 * @brief Cubecell::hasHexDigits
 * @param srcStr
 * @param hexStr
 * @param len
 * @return
 *
 * Cherche et extrait la 1ère chaine hexadécimale  de 'len' digits trouvée dans 'srcStr'
 */
bool Cubecell::hasHexDigits(const QString &srcStr, QString &hexStr, int len)
{
    QRegularExpression matcher;
    QRegularExpressionMatch match;

    // Motif pour chercher 'len' digits hexadecimaux consécutifs
    matcher.setPattern(tr("[0-9A-F]{%1}").arg(QString::number(len)));

    // Met en place une recherche insensible à la casse
    matcher.setPatternOptions(QRegularExpression::CaseInsensitiveOption);

    // Effectue la recherche
    match = matcher.match(srcStr);

    // Extrait et retourne la chaine si trouvée
    if (match.hasMatch())
    {
        hexStr = match.captured();
        return true;
    }
    return false;
}

bool Cubecell::hasDecDigits(const QString &srcStr, QString &decStr)
{
    QRegularExpression matcher;
    QRegularExpressionMatch match;

    // Motif pour chercher 'len' digits décimaux consécutifs
    matcher.setPattern("[0-9]+");

    // Met en place une recherche insensible à la casse
    matcher.setPatternOptions(QRegularExpression::CaseInsensitiveOption);

    // Effectue la recherche
    match = matcher.match(srcStr);

    // Extrait et retourne la chaine si trouvée
    if (match.hasMatch())
    {
        decStr = match.captured();
        return true;
    }
    return false;

}

void Cubecell::onResponseReceived(const CubecellWorker::cmd_id_t &idCmd, const QString &s)
{
    QString response;
    QString hexStr;
    QString decStr;

    qDebug() << "Response cmd<" << _workerThread.getATCommand(idCmd) << "> -> " << s;
        switch (idCmd) {
        case CubecellWorker::CHIP_ID :
            response = s.mid(s.indexOf("+ChipID:"));
            if( hasHexDigits(response, hexStr, 12) ) {
                qDebug() << "ChipID : " << hexStr;
                emit chipIdReady(hexStr);
            }
            break;

        case CubecellWorker::DEV_EUI :
            response = s.mid(s.indexOf("+DevEui:"));
            if( hasHexDigits(response, hexStr, 16) ) {
                qDebug() << "DevEui : " << hexStr;
                emit devEuiReady(hexStr);
            }
            break;

        case CubecellWorker::APP_EUI :
            response = s.mid(s.indexOf("+AppEui:"));
            if( hasHexDigits(response, hexStr, 16) ) {
                qDebug() << "AppEui : " << hexStr;
                emit appEuiReady(hexStr);
            }
            break;


        case CubecellWorker::APP_KEY :
            response = s.mid(s.indexOf("+AppKey:"));
            if( hasHexDigits(response, hexStr, 32) ) {
                qDebug() << "AppKey : " << hexStr;
                emit appKeyReady(hexStr);
            }
            break;


        case CubecellWorker::DUTY_CYCLE :
            response = s.mid(s.indexOf("+DutyCycle:"));
            if( hasDecDigits(response, decStr) ) {
                qDebug() << "DutyCycle : " << decStr;
                emit dutyCycleReady(decStr);
            }
            break;

        case CubecellWorker::WAIT_JOIN :
            if(s.indexOf("joined") != -1)
            {
                emit joinReady("joined");
                emit joined();
            } else {
                emit joinReady("not joined");
            }
            break;

        case CubecellWorker::SEND_HEX :
        case CubecellWorker::WAIT_SEND_HEX :
            if(s.indexOf("+Send Hex Data:") != -1) {
                response = s.mid(s.indexOf("+Send Hex Data:"));
                response.truncate(response.indexOf('\r'));
                emit sendHexReady(response);
                emit uplinkDone();
            } else {
                emit sendHexReady(s);
            }
            break;

        case CubecellWorker::IS_TX_CONFIRMED :
            if(s.indexOf("+IsTxConfirmed:") != -1) {
            } else {
            }
            break;

        default:
            break;

        }
}

void Cubecell::onTimeOut(const CubecellWorker::cmd_id_t &id, const QString &err)
{
    switch (id) {
    case CubecellWorker::CHIP_ID :
        emit chipIdReady("??????????????");
        break;

    case CubecellWorker::DEV_EUI :
        emit devEuiReady("????????????????");
        break;

    case CubecellWorker::APP_EUI :
        emit appEuiReady("????????????????");
        break;


    case CubecellWorker::APP_KEY :
        emit appKeyReady("????????????????????????????????");
        break;


    case CubecellWorker::DUTY_CYCLE :
        emit dutyCycleReady(0);
        break;

    case CubecellWorker::WAIT_JOIN :
        emit joinReady("not joined");
        break;

    case CubecellWorker::SEND_HEX :
        break;

    case CubecellWorker::WAIT_SEND_HEX :
        emit sendHexReady("");
        break;

    }
}
