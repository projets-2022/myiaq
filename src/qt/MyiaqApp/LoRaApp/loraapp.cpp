#include <cmath>

#include <QDebug>
#include <QSettings>
#include <QFile>


#include "loraapp.h"

LoraApp::LoraApp(QObject *parent) : QObject(parent)
{
    _ttn = new Cubecell(_SERIAL_PORT);

    connect(_ttn, &TTN::joined, this, &LoraApp::_onTtnJoined);
    connect(_ttn, &TTN::uplinkDone, this, &LoraApp::_onUplinkTtnDone);

    //connect(&_sensors, &SensorsDataFeed::stateChanged, this, &LoraApp::_onStateChanged);
    connect(&_sensors, &SensorsDataFeed::dataReady, this, &LoraApp::_onSensorsDataReady);

    _loraPayload = QByteArray(4, 0x00);

    // Définir le format et le chemin vers le fichier de paramétrage :
    // %USERPROFILE%\AppData\Roaming\CERI\MyIAQ.ini
    // $HOME/.config/CERI/MyIAQ.ini
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "CERI", "MyIAQ");
    settings.setIniCodec("UTF-8");

    // Créer fichier .ini de base s'il nexiste pas
    if ( !QFile::exists(settings.fileName())) {
        qDebug() << "Fichier .ini manquant => création d'un fichier .ini par défaut";

        // Spécification des paramètres LoRa
        settings.beginGroup("LoRa");
        settings.setValue("AppEUI", "4D794941512D3232"); // "myiaq-2022" en ASCII
        settings.setValue("DevEUI", "");
        settings.setValue("AppKey", "00112233445566778899AABBCCDDEEFF");
        settings.setValue("DutyCycle", 60);
        settings.endGroup();

        // Ecriture de la config dans le fichier .ini
        settings.sync();
    }

    QString appEui = settings.value("LoRa/AppEUI").toString();
    QString appKey = settings.value("LoRa/AppKey").toString();
    QString devEui = settings.value("LoRa/DevEUI").toString();

    _ttn->initOTAA(appEui, appKey, devEui);

    _ttn->joinOTAA();

    connect(&_tmr, &QTimer::timeout, this, &LoraApp::_onTmrTimeout);
    _tmr.setInterval(settings.value("LoRa/DutyCycle").toInt() * 1000);

    /*
     * Dans le pire des cas (SF12-125kHz) l'émission de 4 octets prend ~1320ms
     * => la fair policy de TTN autorise 30s d'émission par jour
     * => donc on peut envoyer au minimum 30000/1320=22 message par jour i.e. ~1 par heure
     */
}

/**
 * @brief LoraApp::makePayload
 * @param mqttValue
 * @return
 *
 * Construit le payload LoRa (<- trame de 4 octets) à partir des données publiées sur le broker mqtt local (<- objet JSON).
 * Format paylaod LoRa :
 *  +----------+----------+----------+----------+----------+----------+---------+---------+
 *  | Tx10°C[8]| Tx10°C[7]| Tx10°C[6]| Tx10°C[5]| Tx10°C[4]| Tx10°C[3]|Tx10°C[2]|Tx10°C[1]|
 *  |   (msb)  |          |          |          |          |          |         |         |
 *  +----------+----------+----------+----------+----------+----------+---------+---------+
 *  | Tx10°C[0]|  HR%[6]  |  HR%[5]  |  HR%[4]  |  HR%[3]  |  HR%[2]  |  HR%[1] |  HR%[0] |
 *  |   (lsb)  |   (msb)  |          |          |          |          |         |         |
 *  +----------+----------+----------+----------+----------+----------+---------+---------+
 *  |CO2ppm[15]|CO2ppm[14]|CO2ppm[13]|CO2ppm[12]|CO2ppm[11]|CO2ppm[10]|CO2ppm[9]|CO2ppm[8]|
 *  |   (msb)  |          |          |          |          |          |         |         |
 *  +----------+----------+----------+----------+----------+----------+---------+---------+
 *  | CO2ppm[7]| CO2ppm[6]| CO2ppm[5]| CO2ppm[4]| CO2ppm[3]| CO2ppm[2]|CO2ppm[1]|CO2ppm[0]|
 *  |          |          |          |          |          |          |         |  (lsb)  |
 *  +----------+----------+----------+----------+----------+----------+---------+---------+
 *  avec :
 *  * Tx10°C : température exprimée en dizièmes de °C (valeur entière)
 *  * HR%    : humidité en % (valeur entière)
 *  * CO2ppm : taux de CO2 exprimé en ppm (valeur entière)
 *  Ex. :
 *  {"T": {"val":31.1, "unit": "°C"}, "HR": {"val":24.9, "unit": "%"},"CO2": {"val":395.75, "unit": "ppm"},"LUX": {"val":978.25, "unit": "lux"}}
 *  => 311*1/10°C, 25%, 396ppm => 0x137, 0x19, 0x018c => 000[1 0011 011][1 [X]001 1001] [0000 0001] [1000 1100] => [0x9B, 0x99, 0x01, 0x8C]
 *
 */
QByteArray LoraApp::_makePayload(QJsonObject& mqttValue)
{
    QJsonObject::iterator itJObj;
    QJsonObject sensorData;
    QHash<QString, double> values = {{"T", 0.0},  {"HR", 0.0}, {"CO2", 0.0}};
    QHash<QString, double>::Iterator itHash;
    QByteArray payload;

    itHash = values.begin();
    while(itHash != values.end()) {
        itJObj = mqttValue.find(itHash.key());
        if( itJObj != mqttValue.end()) {
            sensorData = itJObj->toObject();
            qDebug() << itHash.key() << " : " << sensorData.value("val").toDouble() << sensorData.value("unit").toString();
            itHash.value() = sensorData.value("val").toDouble();
        }
        itHash++;
    }

    int tX10 = static_cast<int>(rint(values.value("T")*10));
    int hum = static_cast<int>(rint(values.value("HR")));
    int co2 = static_cast<int>(rint(values.value("CO2")));
    payload.push_back(tX10 >> 1);
    payload.push_back(((tX10 & 1) << 7) | hum);
    payload.push_back((co2 >> 8) & 0xff);
    payload.push_back(co2 & 0xff);

    return payload;
}

/*
void LoraApp::_onStateChanged(IDataFeed::FeedState state)
{

    if(state == IDataFeed::FeedState::Ready) {
        //
    }
}
*/

void LoraApp::_onSensorsDataReady()
{
    QJsonObject jobj = _sensors.pull();
   _loraPayload = _makePayload(jobj);
}

void LoraApp::_onTtnJoined()
{
    _tmr.start();
}

void LoraApp::_onUplinkTtnDone()
{
    qDebug() << "Uplink done";
}

void LoraApp::_onTmrTimeout()
{
    qDebug() << "Uplink triggered";
    _ttn->send(_loraPayload, 1, true);
}
