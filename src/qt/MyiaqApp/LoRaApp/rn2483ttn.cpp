#include <QDebug>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QThread>
#include <QTimer>

#include "rn2483ttn.h"

RN2483TTN::RN2483TTN() : _bcm(Bcm2835Wrapper::getInstance())
{
    _commPort = new QSerialPort("/dev/ttyAMA0");

    // Mise en place du slot
    connect(_commPort, &QSerialPort::readyRead, this, &RN2483TTN::onReadyRead);

    // Configuration du port série
    _commPort->setBaudRate(QSerialPort::Baud57600);
    _commPort->setDataBits(QSerialPort::Data8);
    _commPort->setParity(QSerialPort::NoParity);
    _commPort->setStopBits(QSerialPort::OneStop);
    _commPort->setFlowControl(QSerialPort::NoFlowControl);

    // Ouverture du port série
    if (_commPort->open(QSerialPort::ReadWrite)) {
        qDebug() << "Ouverture port série OK"  ;

        // Reset du RN2483
        hardReset();

        // Modification de la vitesse par défaut (57600 -> 9600) :
        autoBauds();
    } else {
        qDebug() << "Erreur ouverture port série"  ;
    }
}

bool RN2483TTN::initOTAA(QString appEui, QString appKey, QString devEui)
{
    bool ret = true;
    QRegularExpression hexMatcher;
    QRegularExpressionMatch match;

    hexMatcher.setPatternOptions(QRegularExpression::CaseInsensitiveOption);

    // Sauvegarde appEui ssi c'est une chaine hexa de 16 caractères
    hexMatcher.setPattern("^[0-9A-F]{16}$");
    match = hexMatcher.match(appEui);
    if(match.hasMatch()) {
        _appEui = appEui;
    } else {
        ret = false;
    }

    // Sauvegarde appKeyi ssi c'est une chaine hexa de 32 caractères
    hexMatcher.setPattern("^[0-9A-F]{32}$");
    match = hexMatcher.match(appKey);
    if(match.hasMatch()) {
        _appKey = appKey;
    } else {
        ret = false;
    }

    // Sauvegarde appEui ssi c'est une chaine hexa de 16 caractères
    if(devEui.isNull()) {
        _devEui = getHwEui();
    } else {
        hexMatcher.setPattern("^[0-9A-F]{16}$");
        match = hexMatcher.match(devEui);
        if(match.hasMatch()) {
            _devEui = devEui;
        } else {
            ret = false;
        }
    }

    qDebug() << "appEui : "  << _appEui;
    qDebug() << "appKey : "  << _appKey;
    qDebug() << "devEui : "  << _devEui;

    // Si OK Alors on envoie ça sur le RN2483 pour configurer l'OTAA
    if( ret) {
        sendCmd("mac set deveui " + _devEui);

        sendCmd("mac set appeui " + _appEui);

        sendCmd("mac set appkey " + _appKey);

        sendCmd("mac set sync 34");

        sendCmd("mac set adr on");

        //sendCmd("mac save");
    }

    return ret;
}

bool RN2483TTN::joinOTAA()
{
    sendCmd("mac join otaa");

    bool ret = waitForResponse("accepted");

    return ret;
}

int RN2483TTN::send(QString payload, int port, bool confirm)
{
    if(confirm) {
        sendCmd("mac tx cnf " + QString::number(port) + " " + payload);
    } else {
        sendCmd("mac tx uncnf " + QString::number(port) + " " + payload);
    }
    return 0;
}

QString RN2483TTN::getHwEui()
{
    return "0004A30B001AF3D8"; // Pictail Autard
}

bool RN2483TTN::sendCmd(QString cmd, QString expectedResp, commErr_t &err)
{
    bool ret = false;

    if(! isTransactionInProgress) {

    } else {
        err = TRANSACTION_IN_PROGRESS;
    }

    return ret;
}


bool RN2483TTN::sendCmd(QString cmd, QString expectedResp)
{
    QByteArray line;
    bool ret = false;
    QRegularExpression okMatcher(expectedResp, QRegularExpression::CaseInsensitiveOption);
    QRegularExpressionMatch match;

    cmd += "\r\n";

    // On vide le buffer de reception
    flushRx();

    // On envoie la commande sur le port série
    _commPort->write(cmd.toStdString().c_str());
    _commPort->flush();
    qDebug() << "commande envoyée : " << cmd;

    return waitForResponse(expectedResp);
}

void RN2483TTN::flushRx()
{
    _commPort->readAll();
    _respList.clear();
}

bool RN2483TTN::waitForResponse(QString respExpected)
{
    bool isOK = false;
    int retry = 0;
    QString resp;
    QRegularExpression okMatcher(respExpected, QRegularExpression::CaseInsensitiveOption);
    QRegularExpressionMatch match;

    while( !isOK && (retry++ < 10) ) {
        if ( !_respList.empty() ) {
            // On récupère 1a 1ère ligne reçue en réponse
            resp = _respList.first();
            _respList.removeFirst();
            qDebug() << "ligne lue : " << resp;

            // On teste si cette réponse correspond à la réponse attendue
            match = okMatcher.match(resp);
            if(match.hasMatch()) {
                isOK = true;
            }
        } else {
            QThread::msleep(100);
        }
    }

    return isOK;
}

/**
 * @brief TtnRN2483::autoBauds
 *
 * Change la vitesse de communication avec le module.
 * La procédure consiste a envoyer un "break" sur la ligne série puis à envoyer l'octet 0x55 à la vitesse désirée.
 * Un "break" consistant à forcer la ligne à 0 pendant une durée supérieure à celle nécessaire pour envoyer un caractère
 * à la vistesse actuelle, on pourra le matérialiser par l'envoi de 'loctet 0x00 à la plus petite vitesse possible (i.e. 1200bauds)
 * ( voir https://www.microchip.com/forums/m901235.aspx)
 */
void RN2483TTN::autoBauds()
{
    bool isOK = false;

    // Try a maximum of 10 times with a 1 second delay
    for (uint8_t i=0; i<10 && !isOK; i++)
    {

        // Modification de la vitesse par défaut (57600 -> 9600) :
        // . Envoi d'un break (simulé par l'envoi de la valeur 0 à la vitesse minimale)
        _commPort->setBaudRate(QSerialPort::Baud1200);
        _commPort->write("\x00", 1);
        // . Envoi de la valeur 0x55 à la vitesse désirée
        _commPort->setBaudRate(QSerialPort::Baud9600);
        _commPort->write("\x55\r\n", 3);

        // Lecture de la version du firmware pour s'assurer que le changement de vitesse est effectif
        isOK = sendCmd("sys get ver", "RN2483");
        qDebug() << "isOK : " << isOK;
    }
}

void RN2483TTN::hardReset()
{
    _bcm.gpio_clr(_RN2483_RESET_GPIO);
    _bcm.gpio_set(_RN2483_RESET_GPIO);
    QThread::msleep(250);

}

void RN2483TTN::onReadyRead()
{
#if 1
    QByteArray line = "<no newline found>";

    if(_commPort->canReadLine()) {
        line = _commPort->readAll().trimmed();
        _respList.append(line);
        qDebug() << "onReadyRead : "  << line;
    }
#else
    QByteArray ba = m_commPort->readAll();
    qDebug() << "readyRead : " << ba.toHex().data();
#endif

}
