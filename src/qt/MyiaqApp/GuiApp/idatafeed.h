#ifndef IDATAFEED_H
#define IDATAFEED_H

#include <QObject>
#include <QJsonObject>
#include <qqml.h>

class IDataFeed : public QObject
{
    Q_OBJECT

public:
    explicit IDataFeed(QObject *parent = nullptr);
    Q_INVOKABLE virtual void push(QJsonObject value) = 0;
    Q_INVOKABLE virtual QJsonObject pull() = 0;

signals:
    void dataReady(void);
};

#endif // IDATAFEED_H
