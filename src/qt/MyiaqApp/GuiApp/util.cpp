#include <stdio.h>
#include <qdebug.h>

#include "util.h"
#include <QNetworkInterface>


Util::Util(QObject *parent) : QObject(parent)
{
}

QString Util::getIpAddress(QString ifName)
{
    QString ifIpAddr;

    ifIpAddr.clear();

    foreach (const QNetworkInterface &netInterface, QNetworkInterface::allInterfaces()) {
        QNetworkInterface::InterfaceFlags flags = netInterface.flags();
        QNetworkInterface::InterfaceType type = netInterface.type();
        if( (bool)(flags & QNetworkInterface::IsRunning) ){
            foreach (const QNetworkAddressEntry &address, netInterface.addressEntries()) {
                if(address.ip().protocol() == QAbstractSocket::IPv4Protocol
                        && ((type == QNetworkInterface::Ethernet) ||(type == QNetworkInterface::Wifi))
                        && (netInterface.humanReadableName() == ifName)
                        )
                {
                    ifIpAddr = address.ip().toString();
                }
            }
        }
    }

    return ifIpAddr;
}

QString Util::getIpAddress(IfType type)
{
    if( type == ETHERNET) {
        return getIpAddress(QNetworkInterface::Ethernet);
    } else {
        return getIpAddress(QNetworkInterface::Wifi);
    }
}

QString Util::getIpAddress(QNetworkInterface::InterfaceType type)
{
    QString ifIpAddr;

    ifIpAddr.clear();

    foreach (const QNetworkInterface &netInterface, QNetworkInterface::allInterfaces()) {
        QNetworkInterface::InterfaceFlags flags = netInterface.flags();
        qDebug() << netInterface.humanReadableName() << " -> " << netInterface.type() << "(" << flags << ")";
        if( (flags & QNetworkInterface::IsRunning)
             && (flags & QNetworkInterface::IsUp)
             ) {
            foreach (const QNetworkAddressEntry &address, netInterface.addressEntries()) {
                if(address.ip().protocol() == QAbstractSocket::IPv4Protocol
                        && (netInterface.type() == type)
                        )
                {
                    ifIpAddr = address.ip().toString();
                }
            }
        }
    }
    return ifIpAddr;

}



/**
 * @brief computeScreenBrightness
 * @param mesureLux
 * @return
 *
 * Calcule le niveau de luminosité de l'écran en fonction de la luminosité ambiante.
 * Une hystérésis est appliquée afin d'éviter des scintillements lors d'évolutions
 * minimes de la luminosité ambiante aux alentours des seuils de changement de
 * la luminosité de l'écran
 *
 * Source : https://forum.arduino.cc/t/hysteresis/506190/26
 */
int Util::computeScreenBrightness(double mesureLux) {
    const double MAX_INDOOR_LUX = 2560.0;
    const double LUX_MARGIN = 5.0;
    const int LUX_LEVELS = 256;
    static int currentOutputLevel = 0;

    if (mesureLux < LUX_MARGIN) {
        currentOutputLevel = 0;
    } else if (mesureLux > MAX_INDOOR_LUX) {
        currentOutputLevel = 255;
    } else {
        // Calcule la marge inférieure pour le niveau de luminosité actuel
        double lb =   (MAX_INDOOR_LUX / LUX_LEVELS ) * currentOutputLevel  ;
        if ( currentOutputLevel > 0 ) lb -= LUX_MARGIN  ;   // subtract margin

        // Calcule la marge supérieure pour le niveau de luminosité actuel
        double ub = ((MAX_INDOOR_LUX / LUX_LEVELS ) * (currentOutputLevel + 1 ) )  - 1;
        if ( currentOutputLevel < LUX_LEVELS ) ub +=  LUX_MARGIN;

        // Définit le nouveau niveau de luminosité de l'écran si la luminosité ambiante est en dehors de l'intervalle
        if ( mesureLux < lb || mesureLux > ub ) {
            currentOutputLevel =   mesureLux * LUX_LEVELS / MAX_INDOOR_LUX;
        }

    }

    return currentOutputLevel ;
}
