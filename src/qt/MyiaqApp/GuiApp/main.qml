import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11
import QtQuick.Dialogs 1.2


import "qrc:/"

import tech.lyceebenoit.myiaq 2.0

Window {
   id: dashboard
   visible: true
   width: 800
   height: 480
    //opacity: 0
    
   Component.onCompleted: {
        dashboard.showFullScreen();
       knobT.update(null);
       knobHR.update(null);
       knobCO2.update(null);
   }

   Rectangle {
      id: background
       anchors.fill: parent
       color: "whitesmoke"
   } // Rectangle - id background

   ColumnLayout {
       spacing: 0

      // Zone supérieure affichage (valeurs de capteur)
      Item {
         //DebugRectangle {}
         id: upperArea
         Layout.preferredWidth: 800
         Layout.preferredHeight: 320
         Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

         GridLayout {
            columns:  3
            rows: 3
            anchors.centerIn: parent
            anchors.fill: parent

            Image {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               sourceSize.height: 50
               source: "temp_icon.svg"
            }

            Image {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               sourceSize.height: 50
               source: "humidity_icon.svg"
            }

            Image {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               sourceSize.height: 50
               source: "co2_icon.svg"
            }


            Text {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               text: "Température"
               color : "#299DD2"
               font.bold: true
               //style : Text.Outline
               //styleColor : "black"
            }

            Text {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               text: "Humidité"
               color : "#299DD2"
               font.bold: true
               //style : Text.Outline
               //styleColor : "black"
            }

            Text {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               textFormat: Text.RichText
               text: "CO<sub>2</sub>"
               color : "#299DD2"
               font.bold: true
               //style : Text.Outline
               //styleColor : "black"
            }

            Knob {
               //DebugRectangle {}
               id: knobT
               objectName : "knobT"
               x: 0
               y: 83
               Layout.preferredWidth: 195
               Layout.preferredHeight: 195
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               from: 0
               to: 50
               reverse: false
               stopLow: 19
               stopMid: 24
               stopHigh: 30
               unitText: "°C"
            }

            Knob {
               //DebugRectangle {}
               id: knobHR
               objectName : "knobHR"
               x: 0
               y: 83
               Layout.preferredWidth: 195
               Layout.preferredHeight: 195
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               from:0
               to: 100
               reverse: false
               stopLow: 40
               stopMid: 65
               stopHigh: 85
               unitText: "%"
             }

            Knob {
                 //DebugRectangle {}
                 id: knobCO2
                 x: 0
                 y: 83
                 Layout.preferredWidth: 195
                 Layout.preferredHeight: 195
                 Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                 from:0
                 to: 5000
                 reverse: false
                 stopLow: 600
                 stopMid: 1200
                 stopHigh: 2000
                 unitText: "ppm"
               }

         } // GridLayout

      } // Item - id : upperArea

      // Zone inférieure affichage (bouton OFF, Notification, Date, bouton paramètres)
      Item {
         id: lowerArea
         Layout.preferredWidth: 800
         Layout.preferredHeight: 160

         //DebugRectangle {}

         RowLayout {
            //anchors.fill: parent

            spacing: 5

            // Bouton OFF
            DelayButton {
               id: control
               //DebugRectangle {}
               //anchors.bottom: parent.bottom
               Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
               Layout.preferredWidth: 32
               Layout.preferredHeight: 32
               checked: false
               delay: 5000

               onActivated: {
                  console.debug("Activated!")
                  checked: false
                  Qt.callLater(Qt.quit)
               }

               background: Rectangle {
                  implicitWidth: 32
                  implicitHeight: 32
                  opacity: enabled ? 1 : 0.3
                  color: control.down ? "#17a81a" : "#21be2b"
                  radius: size / 2

                  readonly property real size: Math.min(control.width, control.height)
                  width: size
                  height: size
                  anchors.centerIn: parent

                  Image {
                      id: icon
                      width: 30
                      height: 30
                      anchors.horizontalCenter: parent.horizontalCenter
                      anchors.verticalCenter: parent.verticalCenter
                      source: "On-Off-Switch.svg"
                  }

                  Canvas {
                        id: canvas
                        anchors.fill: parent

                        Connections {
                           target: control
                           onProgressChanged: canvas.requestPaint()
                        }

                        onPaint: {
                           var ctx = getContext("2d")
                           ctx.clearRect(0, 0, width, height)
                           ctx.strokeStyle = "white"
                           ctx.lineWidth = parent.size / 20
                           ctx.beginPath()
                           var startAngle = Math.PI / 5 * 3
                           var endAngle = startAngle + control.progress * Math.PI / 5 * 9
                           ctx.arc(width / 2, height / 2, width / 2 - ctx.lineWidth / 2 - 2, startAngle, endAngle)
                           ctx.stroke()
                        }
                  }
               }
            } // DelayButton - id: control

            // Zone notification
            Column {
                id: idNotification
                Layout.preferredWidth: 400
                Layout.preferredHeight: 160
                opacity: 0.0

                Rectangle {
                    id: alarmCO2
                    anchors.fill: parent
                    radius: 10
                    color: "lavender"
                    border.color: "black"
                    border.width: 5

                    Image {
                        //DebugRectangle {}
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.bottom: parent.verticalCenter
                        sourceSize.height: 50
                        source: "warning-sign.svg"
                    }

                    Text {
                        //DebugRectangle{}
                        id: advice
                        text: "Ouvrir les fenêtres"
                        color : "crimson"
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.verticalCenter
                        font.pointSize: 20
                        font.weight: Font.ExtraBold
                    }
                } // Rectangle - id: alarmCO2
            } // Column - id: notification

            // Zone date
            Column {
                id : today
                Layout.preferredWidth: 320
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               //DebugRectangle {}

               Text {
                  id: idTime
                  //DebugRectangle{}
                  //text: Qt.formatDateTime(new Date(), "ddd d MMMM")
                  color : "#299DD2"
                  font.pointSize: 40
               }

               Text {
                  id: idDate
                  //DebugRectangle{}
                  //text: Qt.formatDateTime(new Date(), "hh:mm")
                  color : "#299DD2"
                  font.pointSize: 18
               }

               Timer {
                  interval: 500
                  running: true
                  repeat: true

                  onTriggered: {
                     //var date = new Date('1995-09-30T03:24:00')
                     var date = new Date()
                     idTime.text = date.toLocaleTimeString(Qt.locale("fr_FR"), "hh:mm:ss")
                     idDate.text = date.toLocaleDateString(Qt.locale("fr_FR"), "ddd d MMMM")
                     //idDate.text = Qt.formatDateTime(date, "ddd d MMMM")
                     //idTime.text = Qt.formatDateTime(date, "hh:mm:ss AP")
                  }
               }

            } // Column

            // Bouton info
            Button {
                id: btnInfo
                //DebugRectangle {}
                onClicked: cfgInfoBox.open()
                //anchors.right: parent.right
                //anchors.bottom: parent.bottom
                Layout.alignment: Qt.AlignRight | Qt.AlignBottom
                Layout.preferredWidth: 32
                Layout.preferredHeight: 32

                background: Rectangle {
                    z: 1
                   implicitWidth: 32
                   implicitHeight: 32
                   opacity: enabled ? 1 : 0.3
                   color: "transparent"
                   radius: size / 2

                   readonly property real size: Math.min(btnInfo.width, btnInfo.height)
                   width: size
                   height: size

                    Image {
                        id: cfgIcon
                        width: 32
                        height: 32
                        anchors.centerIn: parent
                        source: "config_icon.svg"
                     }
                }
            }


         } // RowLayout
      } // Item - id: lowerArea

   } // ColumnLayout

    SensorsDataFeed {
        id: mySensors
        onDataReady: {
            console.log("Data Feed Ready");
            var jsonObject = mySensors.pull();
            console.log("T : " + jsonObject.T.val);
            knobT.update(jsonObject.T.val);
            knobT.unitText = jsonObject.T.unit;
            knobHR.update(jsonObject.HR.val);
            knobHR.unitText = jsonObject.HR.unit;
            knobCO2.update(jsonObject.CO2.val);
            if (knobCO2.value >= knobCO2.stopHigh) {
                Alarm.setVolume(10)
                Alarm.trigger();
                idNotification.opacity = 1.0
            } else if (knobCO2.value >= knobCO2.stopMid) {
                Alarm.reset();
                idNotification.opacity = 1.0
            } else {
                Alarm.reset();
                idNotification.opacity = 0.0
            }
            knobCO2.unitText = jsonObject.CO2.unit;
            //background.color = jsonObject.LUX.val < 100 ? 'black' : 'white';
            let screenBrightness  = Util.computeScreenBrightness(jsonObject.LUX.val);
            console.log("Screen brightness " + screenBrightness);
            background.color = Qt.rgba(screenBrightness/255.0, screenBrightness/255.0, screenBrightness/255.0, 1);
        }
    } // SensorsDataFeed - id: mySensors

    Popup {
        id: cfgInfoBox
        x:10
        y:10
        width: 600
        height: 350
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        //anchors.centerIn: parent

        onClosed : cfgInfoBoxView.setCurrentIndex(0)

        background: Rectangle {
            //anchors.fill: popup
            border.color: "transparent"
            color: "transparent"
        }

        Rectangle {
            id: cfgInfoBoxFrame
            anchors.fill: parent
            radius: 50
            color: "whitesmoke"

            SwipeView {
                id: cfgInfoBoxView

                currentIndex: 0
                anchors.fill: parent
                clip: true // clipping du contenu durant la transition


                Item {
                    id: cfgNetwork

                    Text {
                        anchors.top: parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Réseau"
                        color : "#299DD2"
                        font.italic: true
                        font.bold: true
                        font.underline: true
                        font.pixelSize: 26
                    }

                    Text{
                        anchors.centerIn: parent
                        anchors.fill: cfgInfoBoxView
                        text: "eth0\t: " + Util.getIpAddress(Util.ETHERNET) + "\nwlan0\t: " + Util.getIpAddress(Util.WIFI)
                        color : "#299DD2"
                        font.bold: true

                    }
                } // Item - id: cfgNetwork

                Item {
                    id: cfgTriggers
                    Text {
                        anchors.top: parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Seuils"
                        color : "#299DD2"
                        font.italic: true
                        font.bold: true
                        font.underline: true
                        font.pixelSize: 26
                    }

                    //Zone Seuil
                    RowLayout {
                        id: rowLayout
                        x: 48
                        y: 190
                        width: 560
                        height: 216
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter

                        //Colonne des noms des seuils
                        ColumnLayout {
                            id: columnLayout
                            width: 100
                            height: 100
                            spacing: 20

                            Text {
                                id: element6
                                text: qsTr("     ")
                                font.pixelSize: 15
                            }

                            Text {
                                id: element
                                width: 140
                                height: 40
                                text: qsTr("Seuil CO2")
                                font.pixelSize: 15
                            }

                            Text {
                                id: element1
                                width: 140
                                height: 40
                                text: qsTr("Seuil Température")
                                font.pixelSize: 15
                            }

                            Text {
                                id: element2
                                width: 140
                                height: 40
                                text: qsTr("Seuil Humidité")
                                font.pixelSize: 15
                            }

                        }

                        //Colonne des Seuils minimum de chaque type de mesures
                        ColumnLayout {
                            id: columnLayout1
                            width: 100
                            height: 100

                            Text {
                                text: qsTr("Seuil Minimum")
                                font.pixelSize: 15
                            }

                            SpinBox {
                                id: lowLevelCO2
                                stepSize: 50
                                to: 900
                                from: 400
                                value: 600
                                font.wordSpacing: -0.2
                                onValueModified: {
                                    knobCO2.stopLow = value
                                    console.log(knobCO2.stopLow)
                                }

                            }

                            SpinBox {
                                id: lowLevelTemperature
                                from: 16
                                to: 22
                                value: 19
                                onValueModified: {
                                    knobT.stopLow = value
                                    console.log(knobT.stopLow)
                                }
                            }

                            SpinBox {
                                id: lowLevelHumidity
                                from: 20
                                to: 60
                                value: 40
                                onValueModified: {
                                    knobHR.stopLow = value
                                    console.log(knobHR.stopLow)
                                }
                            }
/*
On pourrait se servir de l'indice HUMIDEX pour déterminer les seuils d'humidité.
* Voir https://blog.francetvinfo.fr/mecaniques-du-ciel/2013/07/22/pourquoi-trouve-t-on-que-le-temps-est-lourd.html
pour déterminer le confort physiologique en fonction de la T°C et de la HR%
* Voir https://community.jeedom.com/t/temperature-ressentie-netatmo/30664/6 pour le codage en JS du calcul de l'humidex :
   t_celcius = 22.8;
   humidite = 55;

   //Calcul pression vapeur eau
   t_kelvin=t_celcius + 273;
   eTs=Math.pow(10,((-2937.4 /t_kelvin)-4.9283* Math.log(t_kelvin)/Math.LN10 +23.5471));
   eTd=eTs * humidite /100;

   //Calcul de l'humidex
   humidex=Math.round(t_celcius + ((eTd-10)*5/9));
   if (humidex < t_celcius) {
       humidex=t_celcius;
   }
*/
                        }

                        //Colonne des Seuils moyen de chaque type de mesures
                        ColumnLayout {
                            id: columnLayout2
                            width: 100
                            height: 100

                            Text {
                                text: qsTr("Seuil Moyen")
                                font.pixelSize: 15
                            }

                            SpinBox {
                                id: midLevelCO2
                                stepSize: 50
                                from: 900
                                to: 1500
                                value: 1200
                                onValueModified: {
                                    knobCO2.stopMid = value
                                    console.log(knobCO2.stopMid)
                                }
                            }

                            SpinBox {
                                id: midLevelTemperature
                                value: 24
                                from: 22
                                to: 27
                                onValueModified: {
                                    knobT.stopMid = value
                                    console.log(knobT.stopMid)
                                }
                            }

                            SpinBox {
                                id: midLevelHumidity
                                from: 60
                                to: 80
                                value: 65
                                onValueModified: {
                                    knobHR.stopMid = value
                                    console.log(knobHR.stopMid)
                                }
                            }
                        }

                        //Colonne des Seuils maximum de chaque type de mesures
                        ColumnLayout {
                            id: columnLayout3
                            width: 100
                            height: 100

                            Text {
                                text: qsTr("Seuil Maximum")
                                font.pixelSize: 15
                            }

                            SpinBox {
                                id: highLevelCO2
                                value: 2000
                                from: 1500
                                to: 5000
                                stepSize: 50
                               onValueModified: {
                                   knobCO2.stopHigh = value
                                   console.log(knobCO2.stopHigh)
                               }
                            }

                            SpinBox {
                                id: highLevelTemperature
                                to: 50
                                from: 27
                                value: 30
                                onValueModified: {
                                    knobT.stopHigh = value
                                    console.log(knobT.stopHigh)
                                }
                            }

                            SpinBox {
                                id: highLevelHumidity
                                value: 85
                                from: 80
                                to: 100
                                onValueModified: {
                                    knobHR.stopHigh = value
                                    console.log(knobHR.stopHigh)
                                }
                            }
                        }
                    }
                } // Item - id: cfgTriggers

                Item {
                    id: cfgTime
                    Text {
                        anchors.top: parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Heure"
                        color : "#299DD2"
                        font.italic: true
                        font.bold: true
                        font.underline: true
                        font.pixelSize: 26
                    }
                    TimePicker {
                        anchors.top: parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        Component.onCompleted: {
                            set(new Date())
                        }
                        onClicked:             {
                            print('onClicked', Qt.formatTime(date, 'h:mm A'))
                        }
                    }
                } // Item - id: cfgTime
            } // SwipeView - id: cfgInfoBoxView

            PageIndicator {
                id: indicator

                count: cfgInfoBoxView.count
                currentIndex: cfgInfoBoxView.currentIndex

                anchors.bottom: cfgInfoBoxView.bottom
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
     }
} // Window
