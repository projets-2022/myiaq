#include "alarm.h"

Alarm::Alarm(QObject *parent) : QObject(parent)
{
    _player = new QMediaPlayer();
    _player->setMedia(QUrl("qrc:/alert1.mp3"));
}

void Alarm::trigger()
{
    if( !_triggered ) {
        _player->setVolume(_volume);
        _player->play();
        _triggered = true;
    }
}

void Alarm::reset()
{
    _triggered = false;
}

void Alarm::setVolume(int volume)
{
    _volume = volume;
}
