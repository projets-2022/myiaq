#include <QJsonDocument>

#include "sensorsdatafeed.h"

SensorsDataFeed::SensorsDataFeed()
    : _mqttClient(nullptr), _subscription(nullptr)
{
    _mqttClient = new QMqttClient();

    connect(_mqttClient, &QMqttClient::connected, this, &SensorsDataFeed::onMqttConnected);
    connect(_mqttClient, &QMqttClient::messageReceived, this, &SensorsDataFeed::onMqttMsgReceived);

    _mqttClient->setHostname(_BROKER_URL);
    _mqttClient->setPort(_BROKER_PORT);
    _mqttClient->connectToHost();
}

void SensorsDataFeed::push(QJsonObject value)
{
    _sensorsValue = value;
}

QJsonObject SensorsDataFeed::pull()
{
    return _sensorsValue;
}

void SensorsDataFeed::onMqttConnected()
{
    QMqttTopicFilter topicFilter = QMqttTopicFilter(_SENSORS_TOPIC);

    if ((! _subscription) // pas de souscription déjà existante
            && topicFilter.isValid() // nom du topic valide
            && (_subscription = _mqttClient->subscribe(topicFilter)) // souscription réussie
            ) {
        qDebug() << "SUCCESS : Subscription to sensors topic";
    } else {
        qDebug() << "ERROR : Subscription to sensors topic";
    }
}

void SensorsDataFeed::onMqttMsgReceived(const QByteArray &message)
{
    QJsonParseError jsonErr;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(message, &jsonErr);

    qDebug() << "jsonErr : " << jsonErr.errorString();

    if( jsonDoc.isObject()) {
        _sensorsValue = jsonDoc.object();
        qDebug() << "SUCCESS : new JSON object read from topic value";
    } else {
         qDebug() << "ERROR : topic value is not a JSON object !";
    }

    emit dataReady();
}


