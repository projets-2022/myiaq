import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

Item {
    id : root

    //property alias spinBoxValue: spinID.value
    property alias spinBoxTitle: titleID.text


    Column {
        Text {
            id: titleID
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Titre"
            font.italic: true
            font.bold: true
            font.underline: true
            font.pixelSize: 26
        }

        GridLayout {
            columns: 3

            Text {
                text: "low"
                Layout.alignment: Qt.AlignHCenter
                font.bold: true
                font.pixelSize: 22
            }

            Text {
                text: "medium"
                Layout.alignment: Qt.AlignHCenter
                font.bold: true
                font.pixelSize: 22
            }

            Text {
                text: "high"
                Layout.alignment: Qt.AlignHCenter
                font.bold: true
                font.pixelSize: 22
            }

            SpinBox {
                id: spinLowID
                value: 0
            }

            SpinBox {
                id: spinMediumID
                value: 0
            }

            SpinBox {
                id: spinHighID
                value: 0
            }

        }

    }


}
