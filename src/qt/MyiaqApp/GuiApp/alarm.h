#ifndef ALARM_H
#define ALARM_H

#include <QObject>
#include <QQmlEngine>
#include <QJSEngine>
#include <QMediaPlayer>

class Alarm : public QObject
{
    Q_OBJECT
public:
    explicit Alarm(QObject *parent = nullptr);
    Q_INVOKABLE void trigger();
    Q_INVOKABLE void reset();
    Q_INVOKABLE void setVolume(int volume);
private:
    bool _triggered = false;
    int _volume = 50;
    QMediaPlayer *_player;
signals:

};

static QObject *alarmProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    Alarm *singletonClass = new Alarm();
    return singletonClass;
}
#endif // ALARM_H
