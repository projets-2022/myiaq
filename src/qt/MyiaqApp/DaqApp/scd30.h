#ifndef SCD30_H
#define SCD30_H

#include <QObject>

#include "bcm2835wrapper.h"

class Scd30 : public QObject
{
    Q_OBJECT
public:
    explicit Scd30(QObject *parent = nullptr);
    int readSensor(double& theTemp, double& theHum, double& theCo2);
signals:

public slots:

private:
    Bcm2835Wrapper& _bcm;
    const uint8_t _SCD30_ADDR = 0x61;
};

#endif // SCD30_H

