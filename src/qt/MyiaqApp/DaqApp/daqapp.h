#ifndef DAQAPP_H
#define DAQAPP_H

#include <QObject>
#include <QTimer>

#include "sensorsdatafeed.h"
#include "scd30.h"
#include "veml7700.h"
//#include "noisefilter.h"

class DaqApp : public QObject
{
    Q_OBJECT
public:
    explicit DaqApp(QObject *parent = nullptr);

private:
    SensorsDataFeed _sensors;
    Scd30 _scd30;
    Veml7700 _veml7700;

    QTimer _cron;

    double _co2;
    double _t;
    double _hr;
    double _lux;
//    NoiseFilter<double, 5> _lux;

private slots:
    void _onTimeout();
    void _onStateChanged(IDataFeed::FeedState state);
};

#endif // DAQAPP_H
