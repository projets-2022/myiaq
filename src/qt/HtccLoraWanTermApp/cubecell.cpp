#include <QDebug>
#include <QRegularExpression>

#include "cubecell.h"

Cubecell::Cubecell(QString serialPort, QObject *parent)
    : QObject(parent), _portName(serialPort), _workerThread(serialPort)
{
    connect(&_workerThread, &CubecellWorker::response, this, &Cubecell::onResponseReceived);
    connect(&_workerThread, &CubecellWorker::timeout, this, &Cubecell::onTimeOut);

    // Désactiver le mode Auto Low Power
    _workerThread.transaction(CubecellWorker::WAKE_UP, "");
    _workerThread.transaction(CubecellWorker::AUTO_LPM, "0");

}

void Cubecell::wakeUp()
{
    _workerThread.transaction(CubecellWorker::WAKE_UP, "");
}

void Cubecell::getChipID()
{
    _workerThread.transaction(CubecellWorker::CHIP_ID, nullptr);
}

void Cubecell::getDevEui()
{
    _workerThread.transaction(CubecellWorker::DEV_EUI, nullptr);
}

void Cubecell::getAppEui()
{
    _workerThread.transaction(CubecellWorker::APP_EUI, nullptr);

}

void Cubecell::getAppKey()
{
    _workerThread.transaction(CubecellWorker::APP_KEY, nullptr);

}

void Cubecell::getDutyCycle()
{
    _workerThread.transaction(CubecellWorker::DUTY_CYCLE, nullptr);

}

void Cubecell::join()
{
    _workerThread.transaction(CubecellWorker::JOIN, "1");
    _workerThread.transaction(CubecellWorker::WAIT_JOIN, "");
}

void Cubecell::sendHex(QString data)
{
    data = data.toUpper(); // Convertit en majuscule
    data.remove(QChar(' ')); // Retire les espaces
    _workerThread.transaction(CubecellWorker::SEND_HEX, data);
    _workerThread.transaction(CubecellWorker::WAIT_SEND_HEX, "");

}

void Cubecell::isTxConfirmed(bool enabled)
{
    if( enabled) {
        _workerThread.transaction(CubecellWorker::IS_TX_CONFIRMED, "1");
    } else {
        _workerThread.transaction(CubecellWorker::IS_TX_CONFIRMED, "0");

    }
}

/**
 * @brief Cubecell::hasHexDigits
 * @param srcStr
 * @param hexStr
 * @param len
 * @return
 *
 * Cherche et extrait la 1ère chaine hexadécimale  de 'len' digits trouvée dans 'srcStr'
 */
bool Cubecell::hasHexDigits(const QString &srcStr, QString &hexStr, int len)
{
    QRegularExpression matcher;
    QRegularExpressionMatch match;

    // Motif pour chercher 'len' digits hexadecimaux consécutifs
    matcher.setPattern(tr("[0-9A-F]{%1}").arg(QString::number(len)));

    // Met en place une recherche insensible à la casse
    matcher.setPatternOptions(QRegularExpression::CaseInsensitiveOption);

    // Effectue la recherche
    match = matcher.match(srcStr);

    // Extrait et retourne la chaine si trouvée
    if (match.hasMatch())
    {
        hexStr = match.captured();
        return true;
    }
    return false;
}

bool Cubecell::hasDecDigits(const QString &srcStr, QString &decStr)
{
    QRegularExpression matcher;
    QRegularExpressionMatch match;

    // Motif pour chercher 'len' digits décimaux consécutifs
    matcher.setPattern("[0-9]+");

    // Met en place une recherche insensible à la casse
    matcher.setPatternOptions(QRegularExpression::CaseInsensitiveOption);

    // Effectue la recherche
    match = matcher.match(srcStr);

    // Extrait et retourne la chaine si trouvée
    if (match.hasMatch())
    {
        decStr = match.captured();
        return true;
    }
    return false;

}

void Cubecell::onResponseReceived(const int &idCmd, const QString &s)
{
    QString response;
    QString hexStr;
    QString decStr;

    qDebug() << "Response cmd<" << idCmd << "> -> " << s;
        switch (idCmd) {
        case ChipID :
            response = s.mid(s.indexOf("+ChipID:"));
            if( hasHexDigits(response, hexStr, 12) ) {
                qDebug() << "ChipID : " << hexStr;
                emit chipIdReady(hexStr);
            }
            break;

        case DevEui :
            response = s.mid(s.indexOf("+DevEui:"));
            if( hasHexDigits(response, hexStr, 16) ) {
                qDebug() << "DevEui : " << hexStr;
                emit devEuiReady(hexStr);
            }
            break;

        case AppEui :
            response = s.mid(s.indexOf("+AppEui:"));
            if( hasHexDigits(response, hexStr, 16) ) {
                qDebug() << "AppEui : " << hexStr;
                emit appEuiReady(hexStr);
            }
            break;


        case AppKey :
            response = s.mid(s.indexOf("+AppKey:"));
            if( hasHexDigits(response, hexStr, 32) ) {
                qDebug() << "AppKey : " << hexStr;
                emit appKeyReady(hexStr);
            }
            break;


        case DutyCycle :
            response = s.mid(s.indexOf("+DutyCycle:"));
            if( hasDecDigits(response, decStr) ) {
                qDebug() << "DutyCycle : " << decStr;
                emit dutyCycleReady(decStr);
            }
            break;

        case WaitJoin :
            if(s.indexOf("joined") != -1)
            {
                emit joinReady("joined");
            } else {
                emit joinReady("not joined");
            }
            break;

        case SendHex :
        case WaitSendHex :
            if(s.indexOf("+Send Hex Data:") != -1) {
                response = s.mid(s.indexOf("+Send Hex Data:"));
                response.truncate(response.indexOf('\r'));
                emit appEuiReady(response);
            } else {
                emit sendHexReady(s);
            }
            break;

        default:
            break;

        }
}

void Cubecell::onTimeOut(const int &id, const QString &err)
{
    switch (id) {
    case ChipID :
        emit chipIdReady("??????????????");
        break;

    case DevEui :
        emit devEuiReady("????????????????");
        break;

    case AppEui :
        emit appEuiReady("????????????????");
        break;


    case AppKey :
        emit appKeyReady("????????????????????????????????");
        break;


    case DutyCycle :
        emit dutyCycleReady(0);
        break;

    case WaitJoin :
        emit joinReady("not joined");
        break;

    case SendHex:
        break;

    case WaitSendHex :
        emit sendHexReady("");
        break;

    }
}
