#ifndef APPIHM_H
#define APPIHM_H

#include <QDialog>
#include <QSerialPortInfo>

#include "cubecell.h"

QT_BEGIN_NAMESPACE
namespace Ui { class AppIHM; }
QT_END_NAMESPACE

class AppIHM : public QDialog
{
    Q_OBJECT

public:
    AppIHM(QWidget *parent = nullptr);
    ~AppIHM();

private slots:
    void on_btnConnect_clicked();
    void onChipIdReady(const QString& s);
    void onDevEuiReady(const QString& s);
    void onAppEuiReady(const QString& s);
    void onAppKeyReady(const QString& s);
    void onDutyCycleReady(const QString& s);
    void onJoinReady(const QString& s);
    void onSendHexReady(const QString& s);

    void on_btnEditDevEui_clicked();
    void on_btnEditAppEui_clicked();
    void on_btnEditAppKey_clicked();
    void on_btnEditDutyCycle_clicked();
    void on_btnApply_clicked();

    void onTextChanged();

    void on_btnRefresh_clicked();

    void on_btnJoin_clicked();

    void on_btnSend_clicked();

private:
    Ui::AppIHM *ui;
    Cubecell * _htcc;
    QString _chipID;
    QString _devEui;
    QString _appEui;
    QString _appKey;
};
#endif // APPIHM_H
