#ifndef EDITIHM_H
#define EDITIHM_H

#include <QDialog>

namespace Ui {
class EditIhm;
}

class EditIhm : public QDialog
{
    Q_OBJECT

public:
    explicit EditIhm(QWidget *parent = nullptr);
    ~EditIhm();

private:
    Ui::EditIhm *ui;
};

#endif // EDITIHM_H
